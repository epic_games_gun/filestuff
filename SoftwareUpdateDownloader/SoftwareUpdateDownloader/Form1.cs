﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace SoftwareUpdateDownloader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public async Task Download(string DownloadLink, string DownloadName)
        {
            await Task.Delay(1000);
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                client.DownloadFileAsync(new Uri(DownloadLink), "C:\\Program Files\\Software Store Update Files\\" + DownloadName);
                Console.WriteLine("Downloaded " + DownloadName );
                FileLabel.Text = "File: " + DownloadName;
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            

            DownloadProgressBar.Value = 0;
            await Task.Delay(1000);
        }

        private async void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            DownloadProgressBar.Value = e.ProgressPercentage;
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }


        private async Task CallDownload()
        {
            ControlBox = false;   
            if (File.Exists("C:\\Search.txt"))
            {
                tabControl1.Visible = false;
            }

            else
            {
                tabControl1.Visible = true;

                await Download("https://raw.githubusercontent.com/CNTowerGUN/SoftwareStoreUpdater/master/OversizeCheck.exe",
                    "OversizeCheck.txt");
                await Task.Delay(3000);
                string OversizeCheck =
                    File.ReadLines("C:\\Program Files\\Software Store Update Files\\OversizeCheck.txt").First();
                if (OversizeCheck == "true")
                {
                    await Download("https://raw.githubusercontent.com/CNTowerGUN/SoftwareStoreUpdater/master/Parts.exe",
                        "Parts.exe");
                   await Download("https://raw.githubusercontent.com/CNTowerGUN/SoftwareStoreUpdater/master/FileName.exe",
                        "FileName.exe");
                   await Download("https://raw.githubusercontent.com/CNTowerGUN/SoftwareStoreUpdater/master/Database.exe",
                        "Database.exe");
                   await Task.Delay(5000);
                    string Parts = File.ReadLines("C:\\Program Files\\Software Store Update Files\\Parts.exe").First();
                    string Name = File.ReadLines("C:\\Program Files\\Software Store Update Files\\FileName.exe")
                        .First();
                    string Database = File.ReadLines("C:\\Program Files\\Software Store Update Files\\Database.exe")
                        .First();
                    Console.WriteLine("Parts: " + Parts + "\n Name: " + Name);
                   await DownloadFile(Name, Parts, Database);
                }
                else
                {
                    Console.WriteLine("Nothing to download");
                }

                this.Close();
            }
        }

        private async Task DownloadFile(string Name, string Parts, string Database)
        {
            var i = 2;
            string PartsNumber = File.ReadLines("C:\\Program Files\\Software Store Update Files\\Parts.exe").First();
            string DatabaseFile = File.ReadLines("C:\\Program Files\\Software Store Update Files\\Database.exe").First();
            Console.WriteLine("Starts at 2, but the number of parts have to be subtracted by 1");
            Console.WriteLine("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name + "." + "part" + i, Name + "." + "part" + i + ".rar");
            
            if (Int32.Parse(PartsNumber) < 11)
            {
               await Download("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name + ".part1.exe",
                    Name + "." + "part1.exe");
            }
            else
            {
              await  Download("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name + ".part01.exe",
                    Name + "." + "part01.exe");
            }

            while (i < Int32.Parse(Parts))
                
            {
                StatusLabel.Text = "Status: (" + i + "/" + PartsNumber + ")";
                if (Int32.Parse(PartsNumber) < 11)
                {
                   await Download(
                        "https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                        "." + "part" + i + ".rar", Name + "." + "part" + i + ".rar");
                    Console.WriteLine("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                                      "." + "part" + i, Name + "." + "part" + i + ".rar");
                }
                else
                {


                    if (i < 10)
                    {
                      await  Download(
                            "https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                            "." + "part" + i + ".rar", Name + "." + "part0" + i + ".rar");
                        Console.WriteLine("0" + i);
                        Console.WriteLine("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" +
                                          Name +
                                          "." + "part0" + i + ".rar", Name + "." + "part0" + i + ".rar");
                        MessageBox.Show("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                                        "." + "part0" + i + ".rar", Name + "." + "part0" + i + ".rar");
                    }
                    else
                    {
                      await  Download(
                            "https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                            "." + "part" + i + ".rar", Name + "." + "part" + i + ".rar");
                        Console.WriteLine("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                                          "." + "part" + i + ".rar", Name + "." + "part" + i + ".rar");
                        Console.WriteLine(i);
                    }
                }

                i++;
            }

            string MainDirectory = "C:\\Program Files\\Software Store Update Files\\";
            if (Int32.Parse(PartsNumber) < 11)
            {
                Process.Start("C:\\Program Files\\Software Store Update Files\\" + Name + "." + "part1.exe").WaitForExit();
            }
            else
            {
                Process.Start("C:\\Program Files\\Software Store Update Files\\" + Name + "." + "part01.exe").WaitForExit();
            }

            if (Int32.Parse(PartsNumber) < 11)
            {
                File.Delete(MainDirectory + Name + "part1.exe");
            }

            else
            {
                File.Delete(MainDirectory + Name + "part01.exe");
            }

            var c = 2;
            while (c < Int32.Parse(PartsNumber))
            {
                if (Int32.Parse(PartsNumber) < 11)
                {
                    File.Delete(MainDirectory + Name + "." + "part" + c + ".rar");
                }
                else
                {
                    if (c < 10)
                    {
                        File.Delete(MainDirectory + Name + "." + "part0" + c + ".rar");
                    }
                    else
                    {
                        File.Delete(MainDirectory + Name + "." + "part" + c + ".rar");
                    }
                }
                c++;
                Console.WriteLine(c);
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (File.Exists("C:\\Search.txt"))
            {
                File.Delete("C:\\Search.txt");
            }
            Application.Exit();
        }

        private string MainDirectoryDew = "C:\\Program Files\\Software Store Update Files\\";
        private  async void SearchButton_Click(object sender, EventArgs e)
        {

            var i = 2;
            string PartsNumber = PartsnumberTextbox.Text;
            string Name = FilenameTextbox.Text;
            string Database = DatabaseNameTextbox.Text;
            string Parts = PartsnumberTextbox.Text;
            Console.WriteLine("Starts at 2, but the number of parts have to be subtracted by 1");
            Console.WriteLine("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                              "." + "part" + i, Name + "." + "part" + i + ".rar");
            if (Int32.Parse(PartsNumber) < 11)
            {
               await Download("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name + ".part1.exe",
                    Name + "." + "part1.exe");
            }
            else
            {
               await Download("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name + ".part01.exe",
                    Name + "." + "part01.exe");
            }

            if (File.Exists(MainDirectoryDew + Name + "." + "part01.exe") !=
                File.Exists(MainDirectoryDew + Name + "." + "part1.exe"))
            {



                while (i < Int32.Parse(Parts))

                {
                    if (Int32.Parse(PartsNumber) < 11)
                    {
                      await  Download(
                            "https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                            "." + "part" + i + ".rar", Name + "." + "part" + i + ".rar");
                        Console.WriteLine("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" +
                                          Name +
                                          "." + "part" + i, Name + "." + "part" + i + ".rar");
                    }
                    else
                    {


                        if (i < 10)
                        {
                            await Download(
                                "https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                                "." + "part" + i + ".rar", Name + "." + "part0" + i + ".rar");
                            Console.WriteLine("0" + i);
                            Console.WriteLine("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" +
                                              Name +
                                              "." + "part0" + i + ".rar", Name + "." + "part0" + i + ".rar");
                            MessageBox.Show("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" +
                                            Name +
                                            "." + "part0" + i + ".rar", Name + "." + "part0" + i + ".rar");
                        }
                        else
                        { 
                           await Download(
                                "https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" + Name +
                                "." + "part" + i + ".rar", Name + "." + "part" + i + ".rar");
                            Console.WriteLine("https://raw.githubusercontent.com/CNTowerGUN/" + Database + "/master/" +
                                              Name +
                                              "." + "part" + i + ".rar", Name + "." + "part" + i + ".rar");
                            Console.WriteLine(i);
                        }
                    }

                    i++;
                }

                string MainDirectory = "C:\\Program Files\\Software Store Update Files\\";
                if (Int32.Parse(PartsNumber) < 11)
                {
                    Process.Start("C:\\Program Files\\Software Store Update Files\\" + Name + "." + "part1.exe")
                        .WaitForExit();
                }
                else
                {
                    Process.Start("C:\\Program Files\\Software Store Update Files\\" + Name + "." + "part01.exe")
                        .WaitForExit();
                }

                if (Int32.Parse(PartsNumber) < 11)
                {
                    File.Delete(MainDirectory + Name + "part1.exe");
                }

                else
                {
                    File.Delete(MainDirectory + Name + "part01.exe");
                }

                var c = 2;
                while (c < Int32.Parse(PartsNumber))
                {
                    if (Int32.Parse(PartsNumber) < 11)
                    {
                        File.Delete(MainDirectory + Name + "." + "part" + c + ".rar");
                    }
                    else
                    {
                        if (c < 10)
                        {
                            File.Delete(MainDirectory + Name + "." + "part0" + c + ".rar");
                        }
                        else
                        {
                            File.Delete(MainDirectory + Name + "." + "part" + c + ".rar");
                        }
                    }

                    c++;
                    Console.WriteLine(c);
                }
            }
            else
            {
                MessageBox.Show("Could not find update");
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process.Start("http://github.com/CNTowerGUN/" + Searchindatabase.Text);
        }
        public static void DeleteDirectory(string path)
        {
            foreach (string directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            if (Directory.Exists(@"C:\Program Files\Software Store Update Files"))
            {
                DeleteDirectory(@"C:\Program Files\Software Store Update Files");
                Directory.CreateDirectory(@"C:\Program Files\Software Store Update Files");
            }
            await CallDownload();
        }
    }
}
